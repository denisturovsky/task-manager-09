package ru.tsc.denisturovsky.tm.service;

import ru.tsc.denisturovsky.tm.api.ICommandRepository;
import ru.tsc.denisturovsky.tm.api.ICommandService;
import ru.tsc.denisturovsky.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
