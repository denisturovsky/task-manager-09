package ru.tsc.denisturovsky.tm.component;

import ru.tsc.denisturovsky.tm.api.ICommandController;
import ru.tsc.denisturovsky.tm.api.ICommandRepository;
import ru.tsc.denisturovsky.tm.api.ICommandService;
import ru.tsc.denisturovsky.tm.constant.TerminalArgument;
import ru.tsc.denisturovsky.tm.constant.TerminalCommand;
import ru.tsc.denisturovsky.tm.controller.CommandController;
import ru.tsc.denisturovsky.tm.repository.CommandRepository;
import ru.tsc.denisturovsky.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        if (processArgument(args)) System.exit(0);
        Scanner scanner = new Scanner(System.in);
        commandController.showWelcome();
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public void close() {
        System.exit(0);
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalArgument.ABOUT:
                commandController.showAbout();
                break;
            case TerminalArgument.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalArgument.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalArgument.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalArgument.HELP:
                commandController.showHelp();
                break;
            case TerminalArgument.VERSION:
                commandController.showVersion();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalCommand.ABOUT:
                commandController.showAbout();
                break;
            case TerminalCommand.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalCommand.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalCommand.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalCommand.HELP:
                commandController.showHelp();
                break;
            case TerminalCommand.VERSION:
                commandController.showVersion();
                break;
            case TerminalCommand.EXIT:
                close();
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

}
