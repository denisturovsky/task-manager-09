package ru.tsc.denisturovsky.tm.api;

import ru.tsc.denisturovsky.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
